=============
Current State
=============

In current state Munbook web application provides it's core functionality - booking module that is lightweight and userfriendly.
The main goal of this project was to create a website with simplistic design and set future milestones for Bookzilla project.

Munbook is currently published here:

http://munbook.pythonanywhere.com

Achieved goals
------------------

- Simplistic design
- Userfriendly interface
- Schedule generating
- Basic validation 
- Fully functional admin panel

Future goals
------------------------

- Providing dynamic loyalty program creator
- Providing My Account view for registered users
- Providing dynamic Offer view
- Developing decorators preventing logged in users from chosen activities




