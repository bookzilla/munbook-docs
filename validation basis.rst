=============
Validation basis
=============

Here i want to show a very basic example of validation procedure. In this case we're going to look at category choice validation.

Controller asks model for all available categories. They are stored in dictionary format. When User sends POST request with chosen category id, controller checks if this category is available. If it's not, user will be sent to category page to make his choice again. If user choice is in available categories, session variable will be set and user will be sent to service choice page.

.. code-block:: python
    :linenos:

    def bookingCategory(request):
        available_categories =  Service.CATEGORIES
        render = render_to_response('booking/category.html', {'categories': available_categories}, context_instance = RequestContext(request))

        if request.method == "POST":
            category_id = request.POST.get('category')

            if category_id not in dict(available_categories):
                return render
            else:
                request.session['category_id'] = category_id
                return redirect('/booking/service/')
        return render



