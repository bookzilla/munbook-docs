.. Munbook documentation master file, created by
   sphinx-quickstart on Mon Jun 27 06:50:40 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Munbook's Documentation
===================================
Munbook is the very first iteration of Bookzilla project, with configuration fitting the needs of small fictional barbershop - Mun.  





.. toctree::
   :maxdepth: 2

   current state
   installation
   user manual
   technologies
   schedule generator
   validation basis

