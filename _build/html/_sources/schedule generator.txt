====================
Schedule Generator
====================
Schedule generator is the heart of Munbook project. It lives in controller laver of MVC pattern inside mun/views/booking.py file and consists of two methods:

- isAvailable - checks if chosen employee is already assigned to reservation at said time
- scheduleIterator - iterates through working hours every 30 minutes and checks if employee is available. Result of this method is a list consisting of dates available for reservation


**isAvailable** method:

.. code-block:: python
    :linenos:

    def isAvailable(start, employee_id, duration):
        employee = Employee.objects.get(pk=employee_id)
        this_period_end = start + timedelta(hours=duration)
        existing_reservations = employee.reservation_set.filter(
            starttime__lt=this_period_end,
            endtime__gt=start
        )
        return not existing_reservations.exists()



**scheduleIterator** method

.. code-block:: python
    :linenos:

    def scheduleIterator_two(date, time, id):
        chosen_date = date
        time_interval = 30  #interval
        working_day_start = 10  #starts working at 10 am
        working_day_end = 17    #ends working at 8 pm
        duration = time  #how long service takes
        start = chosen_date + timedelta(hours = working_day_start)
        end = chosen_date + timedelta(hours = working_day_end)
        availability_table = []
        employee_id = id

        while start <= end - timedelta(hours = duration):
            is_available = isAvailable(start, employee_id, duration)
            if is_available:
                availability_date = [start, start + timedelta(hours = duration)]
                availability_table.append(availability_date)
            start += timedelta(minutes = time_interval)
        return availability_table

Time intervals are customizable.