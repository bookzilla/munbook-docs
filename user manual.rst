
=============
User Manual
=============
Currently you can try Munbook app in two ways:

- Install it on your local machine
- Go to http://munbook.pythonanywhere.com

If you've decided to launch it on your local machine please check out Installation guide!



Things to mention 
-----------------
Munbook app is currently available only in Polish language. Multi language support will be introduced soon.

Booking module usage
--------------------
To start reservation procedure choose *Zarezerwuj termin* option. Then proceed and choose service you are interested in.

Then you have to pick day and time. You should notice that you are limited to 3 months time range.

If you are not logged in user, you will be asked to provide your Email adress. 

Hit *Zarezerwuj* button. Done! You've made your first reservation.