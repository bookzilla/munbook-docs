============
Installation
============

To install and launch Munbook project on a local machine you will need three components:

- Latest Python version
- Django framework
- Django-bootstrap-form plug-in

Python
-------------

Preffered way to install ``Python`` is to download setup wizard directly from their website and install it with ``Add to PATH`` option checked.

It's worth to mention that most Linux and OS X machines have ``Python`` already installed.

Django
--------------

When you've already installed ``Python``, you will be able to use pip command. Open your terminal(Unix)/cmd.exe(Windows) and type in this command:

    $ pip install django

Django is now installed on your machine.

Django-bootstrap-form
--------------

This plugin is used to make Django-forms look nicer. Bootstrap became extremely popular and set new standards in Web Design field.

    $ pip install django-bootstrap-form

Now everything is set up!

Launching website
-------------------

When you've everything setup, you should go to Munbook's main folder(that is the one which contains manage.py file) in your terminal/cmd.exe and type in:

    $ python manage.py runserver

Now Munbook has launched on your machine. By default you should be able to access it at localhost:8000 in your browser.