=============
Technologies
=============
This is a brief description of technologies and logic behind Munbook app.

Django framework
------------------------------------
Munbook web service was written with Python and Django framework. Django enforces MVC design, which makes code much more readable and elegant. 
Thanks to Django it is also much easier to migrate app to other type of database. For demo purposes Munbook uses sqlite database, but for commercial 
use and API development it will be switched to postgresql database.

Jinja 2
------------------------------------
Django uses Jinja 2 template engine for view (template) layer. Jinja allows us to use statements like *extends* or *includes*. 
This way we can avoid html repetition and keep our templates code clean.
By using Jinja we are also provided with possibility of sending variables (custom objects are supported) to view layer.
Jinja is also used in Flask framework.


Django-bootstrap-form
------------------------------------
Django-bootstrap-form is a plugin for Django. It's used to make default Django-forms look prettier. 
Every view that contains forms was provided with django-bootstrap-form tag.

        {% load bootstrap %}

Validating data provided by user is huge part of web development, using django-forms protects us from XSS attacks and potencial problems with conflicts between view and model layers.

Bootstrap 3
------------------------------------
Bootstrap was used to make Munbook look modern and compatible with mobile devices.

CSS
-----------------
Munbook has consistent color scheme. All custom styles were defined inside mun-styles.css file. It contains over 300 lines of CSS styling.

Javascript, Jquery, AJAX
------------------------------------
All these technologies were used inside the booking module for date picking. Date picker is provided by eternicode:
       
        https://github.com/eternicode/bootstrap-datepicker

Whenever user chooses new day, AJAX code is asking controller method to refresh the view with available times for chosen day.
That way page doesn't have to be refreshed and user experience is greatly improved.


